from django.db import models
from django.core.validators import URLValidator

class ChartDatas(models.Model):
    dataLine = models.IntegerField(),
    dataDoughnut = models.IntegerField(),
    dataBar = models.IntegerField(),