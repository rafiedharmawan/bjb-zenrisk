from django.views.generic import View
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import ChartDatas

class HomePageView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'charts.html')

class ChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        data = {
            "labels": ["0.0", "0.2", "0.4", "0.6", "0.8", "1.0"],
            "labels2": ["x","y"],
            "data": [120, 150, 111, 115, 200, 117],
            "data3": [130, 145, 145, 116, 117, 119],
            "data2": [120,200],
        }   

        return Response(data)

    def getDataLine(self):
        data = ChartDatas.objects.values_list('dataLine', flat=True)
        return data




        